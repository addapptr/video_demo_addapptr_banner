//
//  AdManager.m
//  AddApptrDemoProject
//
//  Created by M on 23/07/16.
//  Copyright © 2016 M. All rights reserved.
//

#import "AdManager.h"
#import "AATKit.h"
@interface AdManager() <AATKitDelegate>
@property (nonatomic, strong) id bannerPlacement;
@end

@implementation AdManager

+ (instancetype) sharedInstance {
    static AdManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AdManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //1. Initialise the AATKit
        [AATKit initWithViewController:nil delegate:self andEnableTestModeWithID:154];
        [AATKit debug:YES];
        [AATKit extendedDebug:NO];
        //2. create a banner placement
        self.bannerPlacement = [AATKit createPlacementWithName:@"FirstBannerPlacement" andType:AATKitBanner414x53];
        
        //3. start loading an ad
        [AATKit startPlacementAutoReload:self.bannerPlacement];
    }
    return self;
}

- (void) reloadBannerPlacement {
    [AATKit reloadPlacement:self.bannerPlacement];
}

- (void) AATKitHaveAd:(id)placement {
    //4. display the banner ad if an ad has been loaded
    
}

- (void) AATKitNoAds:(id)placement {
    NSLog(@"No Ads received");
}

static CGFloat tabbarHeight = 49.0;
- (void) positionBannerPlacementOnViewController: (UIViewController*) viewController {
    UIView *bannerView = [AATKit getPlacementView:self.bannerPlacement];
    bannerView.backgroundColor = [UIColor redColor];
    CGPoint bannerPos = CGPointMake(0, viewController.view.bounds.size.height - bannerView.bounds.size.height - tabbarHeight);
    [AATKit setPlacementPos:bannerPos forPlacement:self.bannerPlacement];
    [AATKit setViewController:viewController]; //update the view controller!!!
    [viewController.view addSubview:bannerView];
    [viewController.view bringSubviewToFront:bannerView];
}

@end