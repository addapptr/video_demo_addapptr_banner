//
//  AppDelegate.h
//  AddApptrDemoProject
//
//  Created by M on 10/07/16.
//  Copyright © 2016 M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

