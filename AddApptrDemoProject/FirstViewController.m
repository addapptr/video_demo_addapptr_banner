//
//  FirstViewController.m
//  AddApptrDemoProject
//
//  Created by M on 10/07/16.
//  Copyright © 2016 M. All rights reserved.
//

#import "FirstViewController.h"
#import "AdManager.h"
@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AdManager sharedInstance] positionBannerPlacementOnViewController:self];
}

@end
