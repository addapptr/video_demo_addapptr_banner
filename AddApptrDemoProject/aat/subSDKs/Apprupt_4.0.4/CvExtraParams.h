//
//  CvExtraParams.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#ifndef CV_EXTRA_PARAMS_IFACE
#define CV_EXTRA_PARAMS_IFACE


@interface CvExtraParams : NSObject

- (void) setValue:(NSString *)value forKey:(NSString *)key;
- (id) valueForKey:(NSString *)key;
- (void) addEntriesFromDictionary:(NSDictionary *)values;
- (void) removeAllObjects;
- (void) removeObjectForKey:(NSString *)key;
- (void) removeObjectsForKeys:(NSArray *)keys;
- (id) objectForKey:(id)key;
- (id) objectForKeyedSubscript:(id)key;
- (void) setObject:(id)value forKey:(id<NSCopying>)aKey;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;

@end

#endif