//
//  CvAudience.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CvDefinitions.h"

@interface CvAudience : NSObject

@property (nonatomic,assign) CvGender gender;
@property (nonatomic,strong) NSDate *birthday;
@property (nonatomic,assign) NSUInteger age;
@property (nonatomic,assign) CvRelationshipStatus relationshipStatus;
@property (nonatomic,assign) NSInteger numberOfChildren;

@end
