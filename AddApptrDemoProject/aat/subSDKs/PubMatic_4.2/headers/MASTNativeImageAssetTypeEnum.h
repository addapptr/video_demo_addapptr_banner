//
//  MASTNativeImageAssetTypeEnum.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 27/03/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

typedef enum _MASTNativeImageAssetTypeEnum
{
    // Icon image to rendered in native Ad
    MASTNativeImageAssetTypeIcon        =   1,
    
    
    // Logo image to rendered in native Ad
    MASTNativeImageAssetTypeLogo        =   2,
    
    
    // Main image to rendered in native Ad
    MASTNativeImageAssetTypeMain        =   3
    
} MASTNativeImageAssetTypeEnum;