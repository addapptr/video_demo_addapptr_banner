//
/*
 
 * PubMatic Inc. ("PubMatic") CONFIDENTIAL
 
 * Unpublished Copyright (c) 2006-2014 PubMatic, All Rights Reserved.
 
 *
 
 * NOTICE:  All information contained herein is, and remains the property of PubMatic. The intellectual and technical concepts contained
 
 * herein are proprietary to PubMatic and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 
 * from PubMatic.  Access to the source code contained herein is hereby forbidden to anyone except current PubMatic employees, managers or contractors who have executed
 
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 
 *
 
 * The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 
 * information that is confidential and/or proprietary, and is a trade secret, of  PubMatic.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 
 * OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF PubMatic IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 
 * LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 
 * TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
 
 */


//  MASTNativeImageAsset.h
//  MASTAdView
//

#import "MASTNativeAssetRequest.h"
#import "MASTNativeImageAssetTypeEnum.h"

@interface MASTNativeImageAssetRequest : MASTNativeAssetRequest

/*!
 @property
 @abstract Integer	Type of the image. Possible values:
             1 — icon image;
             2 — logo image;
             3 — main image.
 */
@property(nonatomic,assign) MASTNativeImageAssetTypeEnum subtype;


/*!
 @property
 @abstract Integer	Maximal width of the image in pixels.
 */
@property(nonatomic,assign) NSInteger width;


/*!
 @property
 @abstract Integer	Maximal height of the image in pixels.
 */
@property(nonatomic,assign) NSInteger height;


/*!
 @property
 @abstract Integer	Minimal width of the image in pixels.
           NOTE: Won't be supported in the initial version. Support for this parameter will be added later.

 */
@property(nonatomic,assign) NSInteger minimumWidth;

/*!
 @property
 @abstract Integer	Minimal height of the image in pixels.
           NOTE: Won't be supported in the initial version. Support for this parameter will be added later.
 */
@property(nonatomic,assign) NSInteger minimumHeight;


@end
