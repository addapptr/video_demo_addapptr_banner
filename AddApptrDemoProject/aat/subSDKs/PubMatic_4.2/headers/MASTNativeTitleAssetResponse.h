//
//  MASTNativeTitleAssetResponse.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 27/03/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

#import "MASTNativeAssetResponse.h"

@interface MASTNativeTitleAssetResponse : MASTNativeAssetResponse

/*!
 @property
 @abstract Represents text value of title
 */
@property(nonatomic,assign) NSString* text;

- (instancetype)initWithId:(NSInteger *)assetId withValue:(NSString*)text;

@end
