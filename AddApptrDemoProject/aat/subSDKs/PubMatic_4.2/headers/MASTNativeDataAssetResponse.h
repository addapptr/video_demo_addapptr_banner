//
//  MASTNativeDataAssetResponse.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 27/03/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

#import "MASTNativeAssetResponse.h"
#import "MASTNativeDataAssetTypeEnum.h"

@interface MASTNativeDataAssetResponse : MASTNativeAssetResponse

/*!
 @property
 @abstract Represents data value.
 */
@property(nonatomic,assign) NSString* value;

/*!
 @property
 @abstract Type ID of the data asset as defined in enum MASTNativeDataAssetTypeEnum.
 NOTE: Only "2" (desc) and "3" (rating) are supported for direct native ads in the initial version. Support for all the other values will be added later. You can use all available values for requesting external ads but ensure the external ad feed partner supports them.
 */
@property(nonatomic,assign) MASTNativeDataAssetTypeEnum subtype;

- (instancetype)initWithId:(NSInteger *)assetId withValue:(NSString*)value;


@end
