//
//  MASTNativeImageAssetResponse.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 27/03/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

//#import <Cocoa/Cocoa.h>
#import "MASTNativeAssetResponse.h"

@interface MASTNativeImageAssetResponse : MASTNativeAssetResponse

/*!
 @property
 @abstract Integer	Maximal width of the image in pixels.
 */
@property(nonatomic,assign) NSInteger width;


/*!
 @property
 @abstract Integer	Maximal height of the image in pixels.
 */
@property(nonatomic,assign) NSInteger height;


/*!
 @property
 @abstract NSURL Represents url from where image is to be downloaded.
 */
@property(nonatomic,strong) NSURL *imageURL;

/*!
 @property
 @abstract Integer	Type of the image. Possible values:
 1 — icon image;
 2 — logo image;
 3 — main image.
 */
@property(nonatomic,assign) NSInteger subtype;


- (instancetype)initWithId:(NSInteger *)assetId withWidth:(NSInteger*)width withHeight:(NSInteger*)height andImageURL:(NSString*) imageURL;


@end
