//
//  MASTNativeResponse.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 27/03/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MASTNativeAssetResponse : NSObject

/*!
 @property
 @abstract Unique asset ID, assigned by exchange.
 */
@property(nonatomic,assign) NSInteger assetId;



@end
