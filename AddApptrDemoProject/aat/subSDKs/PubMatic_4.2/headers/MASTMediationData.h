//
//  MASTMediationData.h
//  MASTAdView
//
//  Created by Shrinivas Prabhu on 30/06/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MASTMediationData : NSObject

@property(nonatomic,strong) NSString *adId;
@property(nonatomic,strong) NSString *networkName;
@property(nonatomic,strong) NSString *networkId;
@property(nonatomic,strong) NSString *adSource;

@end
