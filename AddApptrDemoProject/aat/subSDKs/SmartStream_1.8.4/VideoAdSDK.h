#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@protocol VideoAdSDKDelegate <NSObject>

@optional
- (void)advertisingIsPreparingToPlay;
- (void)advertisingIsReady;
- (void)advertisingWillShow;
- (void)advertisingDidHide;
- (void)advertisingClicked;
- (void)advertisingPrefetchingDidComplete;
- (void)advertisingPrefetchingDidCompleteWithAd:(BOOL)adAvailable;
- (void)advertisingNotAvailable;
- (void)advertisingFailedToLoad:(NSError*)error;
- (void)advertisingEventTracked:(NSString*)event;
- (void)advertisingIsAvailable;
@end

@interface VideoAdSDK : NSObject<CLLocationManagerDelegate> {
}

@property (retain, nonatomic) id<VideoAdSDKDelegate> delegate;
@property (assign, atomic) BOOL compatibleEnvironment;
@property (assign, atomic) BOOL allowCellularPrefetching;

+ (VideoAdSDK*)sharedInstance;
+ (VideoAdSDK*)_sharedInstance;

+ (BOOL)registerWithPublisherID:(NSString*)publisherID delegate:(id<VideoAdSDKDelegate>)delegate;
+ (BOOL)playAdvertising;
+ (BOOL)prepareAdvertising;
+ (BOOL)prefetchAdvertising;
+ (void)setForceInterfaceOrientation:(BOOL)force;
+ (void)setForcedInterfaceOrientation:(UIInterfaceOrientation)orientation;
+ (void)setUserAttribute:(NSString*)attribute forKey:(NSString*)key;
+ (void)setDelegate:(id<VideoAdSDKDelegate>)delegate;


@end
