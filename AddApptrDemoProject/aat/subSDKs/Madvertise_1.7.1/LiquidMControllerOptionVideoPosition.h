//
//  LiquidMControllerOptionVideoPosition.h
//  LiquidM-iOS-SDK
//
//  Created by Khaterine Castellano on 07/05/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#ifndef LiquidM_iOS_SDK_LiquidMControllerOptionVideoPosition_h
#define LiquidM_iOS_SDK_LiquidMControllerOptionVideoPosition_h

typedef enum {
    // Positions
    LiquidMVideoInterstitialTop,
    LiquidMVideoInterstitialCenter,
    LiquidMVideoInterstitialBottom
} LiquidMControllerOptionVideoPosition;

#endif