//
//  LiquidMVideoInterstitialViewController.h
//  LiquidM-iOS-SDK
//
//  Created by Khaterine Castellano on 14/05/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiquidMAbstractVideoViewController.h"

@class LiquidMVideoInterstitialViewController;

/*!
 This is the protocol that has to be implemented to set the
 [LiquidMVideoInterstitialViewController delegate] property.
 */
@protocol LiquidMVideoInterstitialViewControllerDelegate <LiquidMAbstractVideoViewControllerDelegate>
 
@end

/*!
 @discussion This class provides an inline and fullscreen video player that is
 powered by LiquidM ads.
 
 **Additional Information**
 
 *Ad classes for video*:
 
 | AdClass                     | Description                                                                              |
 | --------------------------- | ---------------------------------------------------------------------------------------- |
 | LiquidMVideoInterstitial    | An interstitial will be presented with an ad video player and a fullscreen image         |
 
 *Options*: The following key-value pairs can be part of a dictionary that can
 be passed to some of the factory methods for creating a
 LiquidMVideoInterstitialViewController
 
 | Key                               | Value                                                                               |
 | --------------------------------- | ----------------------------------------------------------------------------------- |
 | LiquidMControllerOptionLocation   | the location that can be used for geo-targeting                                     |
 | LiquidMControllerOptionPosition   | determines the positioning of the video player on the view                          |
 | LiquidMControllerOptionTokenTag   | name (NSString) of the tag for looking up the initial token (default: `@"default"`) |
 
 **Note** that you will have to wrap the BOOL into a `NSNumber` for putting it
 into a `NSDictionary` (default is `YES`)
 */

@interface LiquidMVideoInterstitialViewController : LiquidMAbstractVideoViewController

/*!
 @abstract The delegate that gets notified about loading and presentation
 events.
 
 @discussion It is adviced to use the delegate to be at least informed about the
 basic loading events in order to do proper presentation.
 
 @see LiquidMVideoInterstitialViewControllerDelegate
 */
@property (nonatomic, weak) id<LiquidMVideoInterstitialViewControllerDelegate> delegate;

/*!
 @abstract Creates a LiquidMVideoInterstitialViewController with all necessary information.
 
 @param rvc UIViewController that is used to present the ad and all its
 additional features.
 
 @return Fully initialized instance of a LiquidMVideoInterstitialViewController
 
 @see controllerWithOptions:
 */
+ (LiquidMVideoInterstitialViewController *)controllerWithRootViewController:(UIViewController *)rootViewController;

/*!
 @abstract Creates a LiquidMVideoInterstitialViewController with all necessary information.
 
 @param rvc UIViewController that is used to present the ad and all its
 additional features.
 
 @param options A dictionary that contains optional parameters.  It can
 optionally contain the one or more of the key-value pairs listed in the
 overview section.
 
 @return Fully initialized instance of a LiquidMVideoInterstitialViewController
 */
+ (LiquidMVideoInterstitialViewController *)controllerWithRootViewController:(UIViewController *)rootViewController
                                                                     options:(NSDictionary *)optionsDict;
/*!
 @abstract Displays the video interstitial view on the screen.

 @discussion This method should always be called after receiving the ad from the
 server. For video interstitials this will present the ad on screen
 as well.

 @return The returned boolean value confirms if the action can be done or not.

 @see [LiquidMAbstractControllerDelegate controllerDidReceiveAd:]
 */
- (BOOL)presentAd;

/*!
 @abstract Displays the ad view on the screen using the specified
 UIViewController.
 
 @discussion This alters the `rootViewController` and then will call presentAd.
 
 @see presentAd
 */
- (BOOL)presentAdOn:(UIViewController *)vc;



@end
