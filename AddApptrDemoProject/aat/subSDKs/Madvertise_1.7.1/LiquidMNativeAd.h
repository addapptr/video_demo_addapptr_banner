//
//  LiquidMNativeAd.h
//  LiquidM-iOS-SDK
//
//  Created by Conrad Calmez on 19/03/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LiquidMVideoViewController;

/*!
 This class represents a native ad. It offers methods to conveniently fetch data
 in a desired format.
 */
@interface LiquidMNativeAd : NSObject
    
/*!
 @name Data Fetching
 */

/*!
 @abstract Fetch data as a NSString.
 
 @param key The key that will be used for data lookup.
 */
- (NSString *)stringForKey:(NSString *)key;

/*!
 @abstract Fetch data as a NSNumber.
 
 @param key The key that will be used for data lookup.
 */
- (NSNumber *)numberForKey:(NSString *)key;

/*!
 @abstract Fetch data as a NSURL.
 
 @param key The key that will be used for data lookup.
 */
- (NSURL *)URLForKey:(NSString *)key;

/*!
 @abstract Fetch data as a UIImage.
 
 @param key The key that will be used for data lookup.
 
 @discussion The image behind the URL will be downloaded and wrapped in a
 UIImage.
 */
- (UIImage *)imageForKey:(NSString *)key;

/*!
 @abstract Fetch data as a video inside a LiquidMVideoViewController.
 
 @param key The key that will be used for data lookup.
 */
- (LiquidMVideoViewController *)videoForKey:(NSString *)key;

/*!
 @abstract Fetch default click URL.
 
 @discussion This method will report once an impression per ad.
 
 */
- (void)reportImpression;

/*!
 @abstract Convenience method for calling click url.
 */
- (void)openClickThroughURL;

/*!
 @abstract Convenience method for calling assets click url.
 
 @param key The key that will be used for data lookup.
 */
- (void)openClickThroughURLWithKey:(NSString *)key;

/*!
 @abstract Method that returns the click url of the asset
 
 @discussion This method returns the default click url if the asset doesn't have its own click url.
 
 @param key The key that will be used for data lookup.
 */
- (NSURL *)clickThroughURLWithKey:(NSString *)key;

/*!

 @abstract Method that returns the keys of the request
 */
- (NSArray *)allKeys;

/*
 @abstract Method that returns the default click url
 */
- (NSURL *)defaultClickThroughURL;

@end
