//
//  AATConfig.m
//  IntentKit
//
//  Created by Daniel Brockhaus on 30.01.13.
//  Copyright (c) 2013 Daniel Brockhaus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


bool aat_use_Admob=YES;
bool aat_use_AdColony=YES;
bool aat_use_Addapptr=YES;
bool aat_use_AdX=YES;
bool aat_use_Amazon=YES;
bool aat_use_AppLift=YES;
bool aat_use_Applovin=YES;
bool aat_use_Apprupt=YES;
bool aat_use_DFP=YES;
bool aat_use_Facebook=YES;
bool aat_use_Flurry=YES;
bool aat_use_Smaato=YES;
bool aat_use_SmartStream=YES;
bool aat_use_iAd=YES;
bool aat_use_Inmobi=YES;
bool aat_use_LoopMe=YES;
bool aat_use_Madvertise=YES;
bool aat_use_MdotM=YES;
bool aat_use_Millennial=YES;
bool aat_use_MoPub=YES; 
bool aat_use_OpenX=YES;
bool aat_use_Nexage=YES;
bool aat_use_Playhaven=YES;
bool aat_use_PubMatic=YES;
bool aat_use_SmartAd=NO; //currently disabled
bool aat_use_Unity=YES;


#if 0   // please enable this when NOT using Millennial
NSString *MillennialMediaAdWasTapped;
NSString *MillennialMediaAdObjectKey;
NSString *MillennialMediaAdModalWillAppear;
NSString *MillennialMediaAdModalDidDismiss;
#endif
