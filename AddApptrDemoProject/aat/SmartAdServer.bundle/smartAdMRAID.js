document.documentElement.style.webkitTouchCallout = "none";
(function () {
    mraid = {};
    var l = "2.0";
    mraid.callSDK = function (u) {
        setTimeout(function () {
            var v = document.createElement("a");
            v.setAttribute("href", u);
            var w = document.createEvent("HTMLEvents");
            w.initEvent("click", true, true);
            v.dispatchEvent(w)
        }, 0)
    };
    var e = "loading";
    var c = "unknown";
    var p = {
        x: 0,
        y: 0,
        width: 0,
        height: 0
    };
    var q = {
        width: 0,
        height: 0
    };
    var a = 0;
    var j = {
        x: 0,
        y: 0,
        width: 0,
        height: 0
    };
    var n = {
        width: 0,
        height: 0
    };
    var h = true;
    var f =  {
        width: 0,
        height: 0,
        useCustomClose: false,
        isModal: false,
        sasBackgroundColor: "000000",
        sasBackgroundOpacity: "0.7",
    };
    var r = {
        allowOrientationChange: true,
        forceOrientation: "none"
    };
    var g = {
        width: -1,
        height: -1,
        customClosePosition: "top-right",
        offsetX: 0,
        offsetY: 0,
        allowOffscreen: true,
    };
    var m = {};
    var i = null;
    var o = -1;
    var k = {
        x: 0,
        y: 0,
        z: 0
    };
    var s = -1;
    var d;
    var t = {
        intensity: -1,
        interval: -1
    };
    var b = {
        sms: false,
        tel: false,
        calendar: false,
        storePicture: false,
        inlineVideo: false,
        sasSendMessage: true
    };
    mraid.getVersion = function () {
        return l
    };
    mraid.getPlacementType = function () {
        return c
    };
    mraid.getState = function () {
        return e
    };
    mraid.getCurrentPosition = function () {
        return p
    };
    mraid.getMaxSize = function () {
        return q
    };
    mraid.getScale = function () {
        return a
    };
    mraid.getDefaultPosition = function () {
        return j
    };
    mraid.getScreenSize = function () {
        return n
    };
    mraid.getExpandProperties = function () {
        return f
    };
    mraid.setExpandProperties = function (u) {
        if (u.width != undefined) {
            f.width = u.width
        }
        if (u.height != undefined) {
            f.height = u.height
        }
        if (u.useCustomClose != undefined) {
            f.useCustomClose = u.useCustomClose
        }
        if (u.sasBackgroundColor != undefined) {
            f.sasBackgroundColor = u.sasBackgroundColor
        }
        if (u.sasBackgroundOpacity != undefined) {
            f.sasBackgroundOpacity = u.sasBackgroundOpacity
        }
        mraid.useCustomClose(u.useCustomClose);
        mraid.callSDK("sas://setExpandProperties?width=" + f.width + "&height=" + f.height + "&useCustomClose=" + f.useCustomClose + "&isModal=" + f.isModal + "&sasBackgroundColor=" + f.sasBackgroundColor + "&sasBackgroundOpacity=" + f.sasBackgroundOpacity);
        if (u.allowOrientationChange != undefined || u.forceOrientation != undefined || u.orientation != undefined || u.lockOrientation != undefined) {
            mraid.setOrientationProperties(u)
        }
    };
    mraid.getOrientationProperties = function () {
        return r
    };
    mraid.setOrientationProperties = function (u) {
        if (typeof u.orientation === "string") {
            u.forceOrientation = u.orientation
        }
        if (u.lockOrientation != undefined) {
            u.allowOrientationChange = !u.lockOrientation
        }
        if (u.allowOrientationChange != undefined) {
            r.allowOrientationChange = u.allowOrientationChange
        }
        if (u.forceOrientation != undefined) {
            r.forceOrientation = u.forceOrientation
        }
        mraid.callSDK("sas://setOrientationProperties?allowOrientationChange=" + r.allowOrientationChange + "&forceOrientation=" + r.forceOrientation)
    };
    mraid.getResizeProperties = function () {
        return g
    };
    mraid.setResizeProperties = function (u) {
        if (u.width != undefined) {
            g.width = u.width
        }
        if (u.height != undefined) {
            g.height = u.height
        }
        if (u.customClosePosition != undefined) {
            g.customClosePosition = u.customClosePosition
        }
        if (u.offsetX != undefined) {
            g.offsetX = u.offsetX
        }
        if (u.offsetY != undefined) {
            g.offsetY = u.offsetY
        }
        if (u.allowOffscreen != undefined) {
            g.allowOffscreen = u.allowOffscreen
        }
        mraid.callSDK("sas://setResizeProperties?width=" + g.width + "&height=" + g.height + "&customClosePosition=" + g.customClosePosition + "&offsetX=" + g.offsetX + "&offsetY=" + g.offsetY + "&allowOffscreen=" + g.allowOffscreen + "")
    };
    mraid.isViewable = function () {
        return h
    };
    mraid.open = function (u) {
        u = encodeURIComponent(u);
        mraid.callSDK("sas://open?url=" + u)
    };
    mraid.close = function () {
        mraid.closeInlineVideoPlayer();
        mraid.callSDK("sas://close")
    };
    mraid.useCustomClose = function (u) {
        if (u != undefined) {
            mraid.callSDK("sas://useCustomClose?customClose=" + u);
            f.useCustomClose = u
        }
    };
    mraid.expand = function (u) {
        if (u === undefined) {
            mraid.callSDK("sas://expand")
        } else {
            u = encodeURIComponent(u);
            mraid.callSDK("sas://expand?url=" + u)
        }
    };
    mraid.resize = function () {
        mraid.callSDK("sas://resize")
    };
    mraid.addEventListener = function (w, x) {
        var u = m[w];
        if (u == null) {
            m[w] = [];
            u = m[w]
        }
        for (var v in u) {
            if (x == v) {
                return
            }
        }
        u.push(x);
        if (w == "locationChange" && u.length == 1) {
            mraid.enableGPSUpdate()
        }
        if (w == "headingChange" && u.length == 1) {
            mraid.enableHeadingUpdate()
        }
        if (w == "tiltChange" && u.length == 1) {
            mraid.enableTiltUpdate()
        }
        if (w == "shake" && u.length == 1) {
            mraid.enableShakeUpdate()
        }
    };
    mraid.removeEventListener = function (w, x) {
        var u = m[w];
        if (u != null) {
            var v = 0;
            while (v < u.length) {
                if (u[v] == x) {
                    u.splice(v, 1)
                } else {
                    v++
                }
            }
        }
        if (w == "locationChange" && u.length == 0) {
            mraid.disableGPSUpdate()
        }
        if (w == "headingChange" && u.length == 0) {
            mraid.disableHeadingUpdate()
        }
        if (w == "tiltChange" && u.length == 0) {
            mraid.disableTiltUpdate()
        }
        if (w == "shake" && u.length == 0) {
            mraid.disableShakeUpdate()
        }
    };
    mraid.playVideo = function (u, w) {
        if (w === undefined) {
            var v = navigator.userAgent;
            if (v.match(/iPad/i) && parseFloat(v.substr(v.indexOf("OS ") + 3, 3).replace("_", ".")) < 5) {
                mraid.open(u)
            } else {
                sasFullscreenVideoPlayer.play(u)
            }
        } else {
            if (w.autoplay === undefined) {
                w.autoplay = 1
            }
            if (w.sasPosition === undefined) {
                mraid.legacyPlayVideo(u, w)
            } else {
                u = encodeURIComponent(u);
                mraid.callSDK("sas://playVideo?url=" + u + "&sasPosition=" + w.sasPosition + "&autoplay=" + w.autoplay)
            }
        }
    };
    mraid.legacyPlayVideo = function (w, y) {
        mraid.closeInlineVideoPlayer();
        var x = false,
            v = false,
            u = false;
        if (typeof y.autoplay != "undefined") {
            if (typeof y.autoplay === "string" && (y.autoplay == "autoplay" || y.autoplay == "true")) {
                x = true
            } else {
                if (typeof y.autoplay === "boolean") {
                    x = y.autoplay
                }
            }
        }
        if (typeof y.controls != "undefined") {
            if (typeof y.controls === "string" && (y.controls == "controls" || y.controls == "true")) {
                v = true
            } else {
                if (typeof y.controls === "boolean") {
                    v = y.controls
                }
            }
        }
        if (typeof y.loop != "undefined") {
            if (typeof y === "string" && (y.loop == "loop" || y.loop == "true")) {
                u = true
            } else {
                if (typeof y.loop === "boolean") {
                    u = y.loop
                }
            }
        }
        y.autoplay = x;
        y.controls = v;
        y.loop = u;
        if (y.inline.top === undefined) {
            y.inline.top = 0
        }
        if (y.inline.right === undefined) {
            y.inline.right = 0
        }
        if (y.width === undefined) {
            y.width = 0
        }
        if (y.height === undefined) {
            y.height = 0
        }
        if (y.startStyle === undefined) {
            y.startStyle = "normal"
        }
        if (y.stopStyle === undefined) {
            y.stopStyle = "normal"
        }
        i = document.createElement("video");
        if (y.startStyle != "fullscreen") {
            i.setAttribute("webkit-playsinline", "")
        }
        if (y.controls == true) {
            i.setAttribute("controls", "")
        }
        i.setAttribute("style", "position:absolute;top:" + y.inline.top + "px;left:" + y.inline.left + "px;width:" + y.width + "px;height:" + y.height + "px;z-index:100000;");
        i.src = w;
        i.addEventListener("ended", function () {
            if (y.loop == true) {
                i.play()
            }
            mraid.closeInlineVideoPlayer()
        }, false);
        i.addEventListener("webkitendfullscreen", function () {
            mraid.closeInlineVideoPlayer()
        });
        document.body.appendChild(i);
        if (y.autoplay == true) {
            i.load();
            i.play()
        } else {
            i.setAttribute("controls", "")
        }
    };
    mraid.closeInlineVideoPlayer = function () {
        if (i != null) {
            i.pause();
            document.body.removeChild(i);
            i = null
        }
    };
    mraid.isLegacyPlayVideo = function (u) {
        return (!(u.audio === undefined) || !(u.controls === undefined) || !(u.loop === undefined) || !(u.inline === undefined) || !(u.width === undefined) || !(u.height === undefined) || !(u.startStyle === undefined) || !(u.stopStyle === undefined))
    };
    mraid.getOrientation = function () {
        return o
    };
    mraid.getTilt = function () {
        return k
    };
    mraid.getHeading = function () {
        return s
    };
    mraid.getLocation = function () {
        return d
    };
    mraid.enableGPSUpdate = function () {
        mraid.setGPSUpdateState(true)
    };
    mraid.disableGPSUpdate = function () {
        mraid.setGPSUpdateState(false)
    };
    mraid.setGPSUpdateState = function (u) {
        mraid.callSDK("sas://setGPSUpdateState?enabled=" + u)
    };
    mraid.enableHeadingUpdate = function () {
        mraid.setHeadingUpdateState(true)
    };
    mraid.disableHeadingUpdate = function () {
        mraid.setHeadingUpdateState(false)
    };
    mraid.setHeadingUpdateState = function (u) {
        mraid.callSDK("sas://setHeadingUpdateState?enabled=" + u)
    };
    mraid.enableTiltUpdate = function () {
        mraid.setTiltUpdateState(true)
    };
    mraid.disableTiltUpdate = function () {
        mraid.setTiltUpdateState(false)
    };
    mraid.setTiltUpdateState = function (u) {
        mraid.callSDK("sas://setTiltUpdateState?enabled=" + u)
    };
    mraid.enableShakeUpdate = function () {
        mraid.setShakeUpdateState(true)
    };
    mraid.disableShakeUpdate = function () {
        mraid.setShakeUpdateState(false)
    };
    mraid.setShakeUpdateState = function (u) {
        mraid.callSDK("sas://setShakeUpdateState?enabled=" + u)
    };
    mraid.getShakeProperties = function () {
        return t
    };
    mraid.setShakeProperties = function (u) {};
    mraid.supports = function (u) {
        return b[u]
    };
    mraid.createCalendarEvent = function (v) {
        if (v.start != undefined) {
            v.start = mraid.timeStampFromInput(v.start)
        }
        if (v.end != undefined) {
            v.end = mraid.timeStampFromInput(v.end)
        }
        if (v.reminder != undefined) {
            v.reminder = mraid.timeStampFromInput(v.reminder)
        }
        if (v.recurrence != undefined) {
            if (v.recurrence.expires != undefined) {
                v.recurrence.expires = mraid.timeStampFromInput(v.recurrence.expires)
            }
            if (v.recurrence.exceptionDates != undefined) {
                for (var u = 0; u < v.recurrence.exceptionDates.length; u++) {
                    v.recurrence.exceptionDates[u] = mraid.timeStampFromInput(v.recurrence.exceptionDates[u])
                }
            }
        }
        mraid.callSDK("sas://createCalendarEvent?parameter=" + encodeURIComponent(JSON.stringify(v)))
    };
    mraid.request = function (u, v) {
        mraid.callSDK("sas://callPixel?url=" + encodeURIComponent(u))
    };
    mraid.isLandscapeDevice = function () {
        return false
    };
    mraid.timeStampFromInput = function (v) {
        var u = 0;
        if (typeof (v) == "number") {
            return v
        } else {
            if ((typeof v === "string") && v.charAt(0) == "-") {
                return v
            } else {
                if (v && v.getTime) {
                    u = v.getTime()
                } else {
                    if (typeof (v) == "string") {
                        var w = new Date(v);
                        u = w.getTime()
                    }
                }
            }
        }
        return "" + (u / 1000)
    };
    mraid.fireStateChangeEvent = function (w) {
        e = w;
        if (w != "expanded" || w != "resized") {
            mraid.closeInlineVideoPlayer()
        }
        var u = m.stateChange;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](w)
            }
        }
    };
    mraid.fireReadyEvent = function () {
        var u = m.ready;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v]()
            }
        }
    };
    mraid.firePlacementTypeEvent = function (u) {
        c = u
    };
    mraid.fireViewableChangeEvent = function (w) {
        h = w;
        var u = m.viewableChange;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](w)
            }
        }
    };
    mraid.fireSizeChangeEvent = function (x, u) {
        p = {
            x: p.x,
            y: p.y,
            width: x,
            height: u
        };
        var v = m.sizeChange;
        if (v != null) {
            for (var w = 0; w < v.length; w++) {
                v[w](x, u)
            }
        }
    };
    mraid.fireErrorEvent = function (w, x) {
        var u = m.error;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](w, x)
            }
        }
    };
    mraid.firePositionChangeEvent = function (u, v) {
        p = {
            x: u,
            y: v,
            width: p.width,
            height: p.height
        }
    };
    mraid.fireMaxSizeChangeEvent = function (v, u) {
        q = {
            width: v,
            height: u
        }
    };
    mraid.fireScaleEvent = function (u) {
        a = u
    };
    mraid.fireScreenSizeChangeEvent = function (v, u) {
        n = {
            width: v,
            height: u
        }
    };
    mraid.fireDefaultPositionChangeEvent = function (v, z, w, u) {
        j = {
            x: v,
            y: z,
            width: w,
            height: u
        }
    };
    mraid.fireExpandPropertiesChangeEvent = function (u) {
        f = u
    };
    mraid.fireOrientationPropertiesChangeEvent = function (u) {
        r = u
    };
    mraid.fireResizePropertiesChangeEvent = function (u) {
        r = u
    };
    mraid.fireOrientationChangeEvent = function (w) {
        o = w;
        var u = m.orientationChange;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](w)
            }
        }
    };
    mraid.fireTiltChangeEvent = function (u, B, A) {
        k = {
            x: u,
            y: B,
            z: A
        };
        var v = m.tiltChange;
        if (v != null) {
            for (var w = 0; w < v.length; w++) {
                v[w](u, B, A)
            }
        }
    };
    mraid.fireHeadingChangeEvent = function (w) {
        s = w;
        var u = m.headingChange;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](w)
            }
        }
    };
    mraid.fireLocationChangeEvent = function (x, y, w) {
        d = {
            lat: x,
            lon: y,
            acc: w
        };
        var u = m.locationChange;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v](x, y, w)
            }
        }
    };
    mraid.fireShakeEvent = function () {
        var u = m.shake;
        if (u != null) {
            for (var v = 0; v < u.length; v++) {
                u[v]()
            }
        }
    };
    mraid.fireUpdateShakePropertiesEvent = function (u, v) {
        t = {
            intensity: u,
            interval: v
        }
    };
    mraid.fireUpdateSupportsEvent = function (u) {
        b = u
    };
    mraid.sasSendMessage = function (u) {
        mraid.callSDK("sas://sendMessage?message=" + encodeURIComponent(u))
    };
    mraid.closeLinear = function () {
        mraid.sasSendMessage("closeLinear");
        mraid.close()
    }
})();
var sasFullscreenVideoPlayer;
(function () {
    sasFullscreenVideoPlayer = {
        init: function () {
            var a = this;
            this.video = document.createElement("video");
            this.video.setAttribute("style", "display: none; position: fixed; top: -500px; left: 0; width: 100%; height: 1px;");
            window.setTimeout(function () {
                a.video.style.display = "block"
            }, 0);
            document.body.appendChild(this.video)
        },
        play: function (c, a) {
            var b = this;
            if (!b.video) {
                b.init()
            }
            b.video.src = c;
            b.video.addEventListener("canplaythrough", function () {
                b.video.webkitEnterFullscreen()
            }, false);
            b.video.load();
            b.video.play()
        }
    }
})();