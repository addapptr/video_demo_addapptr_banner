var sasSupportsReadyToDisplay = c.autoplay;
var sasSupportsClick = true;
var v, vc, d = document,
    w = window,
    cp = false;
d.addEventListener("touchmove", function (a) {
    a.preventDefault()
});
w.addEventListener("load", load, false);

function addElement(b) {
    console.log("addElement(b) called" + b);
    var a = d.createElement(b);
    d.body.appendChild(a);
    return a
}

function loadWithNativePlayer() {
    console.log("loadWithNativePlayer called");
    w.addEventListener("orientationchange", updateView, false);
    vc = addElement("div");
    vc.addEventListener("click", click, false);
    vc.style.width = w.innerWidth + "px";
    vc.style.height = w.innerHeight + "px";
    updateView();
    mraid.playVideo(c.video.url, {
        sasPosition: c.position,
        autoplay: c.autoplay,
        sasFullscreenOnClick: c.fullscreenOnClick
    })
}

function loadWithHTMLPlayer() {
    console.log("loadWithHTMLPlayer called");
    v = addElement("video");
    vc = addElement("div");
    v.setAttribute("style", "position:absolute;top:0;left:0;");
    v.setAttribute("webkit-playsinline", "");
    vc.setAttribute("style", "position:absolute;top:0;left:0;z-index:50");
    w.addEventListener("orientationchange", updateView, false);
    vc.addEventListener("click", click, false);
    v.addEventListener("ended", close, false);
    v.addEventListener("loadedmetadata", updateView, false);
    v.addEventListener("canplaythrough", canplaythrough, false);
    v.addEventListener("webkitendfullscreen", webkitendfullscreen, false);
    v.addEventListener("abort", error, false);
    v.addEventListener("emptied", error, false);
    v.addEventListener("error", error, false);
    v.src = c.video.url;
    if (c.autoplay) {
        v.play();
        v.pause()
    } else {
        v.setAttribute("controls", "")
    }
    updateView()
}

function load() {
    console.log("load() called");
    if (typeof mraid == "undefined" || c.iosVersion > 5) {
        loadWithHTMLPlayer()
    } else {
        loadWithNativePlayer()
    }
}

function updateView() {
    console.log("updateView");
    if (typeof v != "undefined") {
        vh = c.video.height * w.innerWidth / c.video.width;
        console.log("c.video.height = " + c.video.height);
        console.log("w.innerWidth = " + w.innerWidth);
        console.log("c.video.width = " + c.video.width);
        console.log("video height = " + vh);
        v.style.width = w.innerWidth + "px";
        v.style.height = vh + "px";
        vc.style.width = w.innerWidth + "px";
        vc.style.height = w.innerHeight + "px"

    }
    if (w.orientation == 0 || w.orientation == 180) {
        d.body.setAttribute("style", 'background:url("' + c.backgroundUrl + '");-webkit-background-size:' + w.innerWidth + " " + w.innerHeight);
        if (typeof v != "undefined") {
            if (c.position == 0) {
                v.style.top = 0
            } else {
                if (c.position == 1) {
                    v.style.top = (w.innerHeight - vh) / 2 + "px"
                } else {
                    v.style.top = (w.innerHeight - vh) + "px"
                }
            }
        }
    } else {
        d.body.setAttribute("style", "");
        if (typeof v != "undefined") {
            v.style.top = (w.innerHeight - vh) / 2 + "px"
        }
    }
}

function nativeVideoReadyToDisplay() {
    callSAS("sas:readyToDisplay")
}

function nativeVideoClicked() {
    callSAS("sas:click")
}

function canplaythrough() {
    if (!cp && c.autoplay) {
        callSAS("sas:readyToDisplay");
        cp = true;
        updateView();
        v.play()
    }
}

function webkitendfullscreen() {
    console.log("webkitendfullscreen called");
    window.setTimeout(delayedPlay, 500)
}

function delayedPlay() {
    v.play();
    callSAS("sas:endfullscreen")
}

function error(a) {
    v.pause();
    callSAS("sas:error:" + a.type)
}

function click() {
        console.log("click called");
    if (c.fullscreenOnClick) {
        if (typeof v != "undefined") {
            v.webkitEnterFullscreen()
        }
        callSAS("sas:enterfullscreen")
    } else {
        if (!c.autoplay) {
            if (typeof v != "undefined") {
                v.play()
            }
            callSAS("sas:click")
        } else {
            if (typeof v != "undefined") {
                v.pause()
            }
            callSAS("sas:click")
        }
    }
}

function close() {
    callSAS("sas:close")
};