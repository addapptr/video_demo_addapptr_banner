//
//  AATKit.h
//
//  Created by Daniel Brockhaus on 04.04.12.
//  Copyright (c) 2012-2013 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef AATKITDELEGATE
#define AATKITDELEGATE
@protocol AATKitDelegate<NSObject>

@optional
- (void) AATKitHaveAd:(id) placement;
- (void) AATKitNoAds:(id) placement;	
- (void) AATKitShowingEmpty:(id) placement; // ONLY works for banner placements, NOT for fullscreen
- (void) AATKitPauseForAd;
- (void) AATKitResumeAfterAd;
- (bool) AATKitAppWillHandleURL:(NSURLRequest*) url;	// ignore unless told otherwise by AddApptr support
- (void) AATKitUserEarnedIncentive;						// ignore unless told otherwise by AddApptr support
- (void) AATKitUserEarnedIncentiveOnPlacement: (id) placement;
- (void) AATKitObtainedAdRules: (bool) fromTheServer;
@end
#endif

#ifndef AATKITBANNERALIGN
#define AATKITBANNERALIGN
typedef enum AATKitBannerAlign {
    AATKitBannerTop,
    AATKitBannerBottom,
    AATKitBannerCenter
} AATKitBannerAlign;
#endif

#ifndef AATKITADTYPE
#define AATKITADTYPE
typedef enum AATKitAdType {
    // Generic
    AATKitBanner320x53,
    AATKitBanner375x53,
    AATKitBanner414x53,
    AATKitBanner768x90,
    AATKitBanner300x250,
    AATKitBanner468x60,
    AATKitFullscreen,
    AATKitNativeAd,
    // iAd only
    AATKitBanner480x32,
    AATKitBanner1024x66,
    // House Ad only
    AATKitBanner300x200,
    AATKitBanner90x80,
    AATKitBanner200x200
} AATKitAdType;
#endif


enum AATKitAdNetwork {
    AATAdColony,
    AATAdMob,
    AATAddapptr,
    AATAdX,
    AATAmazon,
    AATAppLift,
    AATApplovin,
    AATApprupt,
    AATDFP,
    AATFacebook,
    AATFlurry,
    AATGroupM,
    AATHouse,
    AATHouseX,
    AATiAd,
    AATInmobi,
    AATLoopMe,
    AATMadvertise,
    AATMdotM,
    AATMillennial,
    AATMobFox,
    AATMoPub,
    AATMock,
    AATPlayhaven,
    AATNexage,
    AATSmaato,
    AATSmartAd,
    AATSmartClip,
    AATSmartStream,
    AATUnity,
    AATPubMatic,
    AATUnknownNetwork
};

enum AATKitNativeAdType {
    AATKitNativeAppInstall,
    AATKitNativeContent,
    AATKitNativeVideo,
    AATKitNativeOther,
    AATKitNativeTypeUnknown
};

#ifndef AATKITCLASS
#define AATKITCLASS
@interface AATKit : NSObject

// init
+ (void) initWithViewController: (UIViewController*)viewcon andDelegate: (id<AATKitDelegate>) delegate;
+ (void) initWithViewController: (UIViewController*)viewcon delegate: (id<AATKitDelegate>) delegate andEnableTestModeWithID: (int) myID;
+ (void) initWithViewController: (UIViewController*)viewcon delegate: (id<AATKitDelegate>) delegate enableRulesCaching: (bool) rulesCaching andInitialRules: (NSString*) initialRules;
+ (void) setViewController:(UIViewController*)con;
+ (void) setDelegate:(id<AATKitDelegate>)delegate;
+ (NSString *) getVersion; // AATKit Version Number, eg. 0104 equals version 1.04
+ (void) debug:(bool) flag;    //default is set to NO
+ (void) extendedDebug: (bool) yesOrNo; //default is set to YES
+ (void) debugShake:(bool) flag; //default is set to YES
+ (NSString*) getDebugInfo;


// placements
+ (id) createPlacementWithName:(NSString *) placementName andType:(AATKitAdType) type;
+ (id) getPlacementWithName:(NSString *) placementName;
+ (void) startPlacementAutoReload:(id) placement;
+ (void) stopPlacementAutoReload:(id) placement;
+ (void) setPlacementViewController:(UIViewController*)con forPlacement:(id) placement;
+ (bool) reloadPlacement:(id) placement; // must not call this if autoreload is enabled for this placement
+ (void) setPlacementSubID:(int) subID forPlacement:(id) placement;
+ (bool) haveAdForPlacement:(id) placement;


// banner only
+ (void) startPlacementAutoReloadWithSeconds:(int) seconds forPlacement:(id) placement;
+ (UIView*) getPlacementView:(id) placement;
+ (CGSize) getPlacementContentSize:(id) placement;
+ (void) setPlacementAlign:(AATKitBannerAlign) align forPlacement:(id) placement;
+ (void) setPlacementPos:(CGPoint) pos forPlacement:(id) placement;
+ (void) setPlacementDefaultImage:(UIImage *) image forPlacement:(id) placement;

// fullscreen only
+ (bool) showPlacement:(id) placement;


//native only
struct AATKitNativeAdRating {
    float value;
    int scale;
    bool hasRating;
};

// native placement lifecycle methods
+ (NSUInteger) currentlyLoadingNativeAdsOnPlacement:(id) placement;
+ (id) getNativeAdForPlacement: (id) placement;
+ (void) appHasAdSpaceForNativePlacement: (id) placement;
+ (bool) isFrequencyCapReachedForNativePlacement: (id) placement;

// native ads configuration methods
+ (void) setViewController: (UIViewController*) con forNativeAd: (id) nativeAd;
+ (void) setTrackingView: (UIView*) trackingView forNativeAd: (id) nativeAd;
+ (void) removeTrackingViewForNativeAd: (id) nativeAd;


// native ads asset getter methods
+ (NSString*)   getNativeAdTitle: (id) nativeAd;
+ (NSString*)   getNativeAdDescription: (id) nativeAd;
+ (NSString*)   getNativeAdImageURL: (id) nativeAd;
+ (NSString*)   getNativeAdIconURL: (id) nativeAd;
+ (NSString*)   getNativeAdCallToAction: (id) nativeAd;
+ (NSString*)   getNativeAdBrandingLogo: (id) nativeAd __deprecated_msg("Will be removed in the next stable release, please use getNativeAdBrandingLogoURL");
+ (NSString*)   getNativeAdBrandingLogoURL: (id) nativeAd;
+ (NSString*)   getNativeAdAdvertiser: (id) nativeAd;

+ (enum AATKitNativeAdType) getNativeAdType: (id) nativeAd;
+ (struct AATKitNativeAdRating) getNativeAdRating: (id) nativeAd;
+ (enum AATKitAdNetwork) getNativeAdNetwork: (id) nativeAd;
+ (bool) isNativeAdExpired: (id) nativeAd;
+ (bool) isNativeAdReady: (id) nativeAd;
+ (bool) isNativeAdVideo: (id) nativeAd;

// promo specials
+ (void) enablePromo;       // enables display of promotional fullscreen ads. THIS IS THE ONLY CALL NEEDED
+ (void) disablePromo;      // disables promotional full screen ads. use this to avoid displaying them when inappropriate
+ (void) preparePromo;      // prepares to display promotional fullscreen ads (ie. load and cache) but do not display yet
                            // enablePromo does this automatically
+ (void) showPromo __deprecated_msg("Will be removed in a future release: Use showPromo: (bool) force instead");
+ (bool) showPromo: (bool) force; // will show a promo if one is available. you need to call preparePromo first. Force = YES enables the promo to be shown more often than once per hour. Force = NO, the promo is only shown once per hour.
+ (void) setPromoViewController:(UIViewController*)viewcon; // use this only if you are using per-placement view controllers

//disable and re-enable ad networks
+ (void) disableAdNetwork:  (enum AATKitAdNetwork) adNetwork;
+ (void) reenableAdNetwork: (enum AATKitAdNetwork) adNetwork;
@end
#endif