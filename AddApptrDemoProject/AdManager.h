//
//  AdManager.h
//  AddApptrDemoProject
//
//  Created by M on 23/07/16.
//  Copyright © 2016 M. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface AdManager : NSObject
+ (AdManager*) sharedInstance;
- (void) positionBannerPlacementOnViewController: (UIViewController*) viewController;
@end
